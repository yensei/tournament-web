<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<div style="margin: 10px;">
	<h4><fmt:message key="usuario.list.title"/></h4>
	<table style="width: 600px" class="reference">
		<tbody>
			<tr>
				<th><fmt:message key="usuario.list.title.col1"/></th>
				<th><fmt:message key="usuario.list.title.col2"/></th>
				<th><fmt:message key="usuario.list.title.col3"/></th>
				<th><fmt:message key="usuario.list.title.col4"/></th>
				<th><fmt:message key="usuario.list.title.col5"/></th>
			</tr>
		<c:forEach var="user" items="${requestScope.usuarios}" varStatus="loopCounter">
			<tr>
				<td><c:out value="${loopCounter.count}" /></td>
				<td><c:out value="${user.id}" /></td>
				<td><c:out value="${user.nombre}" /></td>
				<td><c:out value="${user.apellido}" /></td>
				<td><c:out value="${user.nick}" /></td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
</div>