<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<ul style="list-style:none;line-height:28px;">

	<li>
		<!-- informacion de marketing y descripcion del funcionamiento de la app -->
    	<spring:url value="/index" var="homeUrl" htmlEscape="true" />
		<a href="${homeUrl}">Inicio</a>
	</li>

	<li>
		<!-- muestra los torneos vigentes -->
		<a href="#">Torneos</a>
	</li>

	<li>
		<!-- Muestra puntuaciones de usuarios con sus nicks -->
<%--     	<spring:url value="/viewPerson" var="personListUrl" htmlEscape="true" /> --%>
    	<spring:url value="/viewListUsers" var="users" htmlEscape="true" />
		<a href="${users}">Usuarios</a>
	</li>

	<li>
		<!-- Muestra las programaciones de los diferentes torneos -->
		<a href="#">Programaci&oacute;n</a>
	</li>
	
	<li>
		<!-- contenido multimedia subida por los usuarios -->
		<a href="#">Galer&iacute;a</a>
	</li>
	
	<li>
		<!-- informacion para contratar el servicio -->
		<a href="#">Contacto</a>
	</li>
	

</ul>