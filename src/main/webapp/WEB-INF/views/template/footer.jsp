<%@taglib  prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<spring:message code="url.goto.linkedin.profile" var="my_profile_linkedin"></spring:message>

<hr />
<div class="span-1 prepend-3">&nbsp;</div>
<div class="span-16 last" style="text-align: center" >
	<p>
		<b><fmt:message key="template.footer.title"/></b>-<fmt:message key="template.footer.rights"/>
		<br/>
		<span class="title"><fmt:message key="template.footer.author"/></span> 
		<br/>
	</p>
	
</div>
<div style="float: right; margin-right: 50px;">
	<a href="${my_profile_linkedin}" target="_blank">
			<img src="https://static.licdn.com/scds/common/u/img/webpromo/btn_profile_bluetxt_80x15_es_ES.png?locale=" 
			width="80" height="15" border="0" alt="Ver el perfil de Julio Mu&ntilde;oz en LinkedIn">
    </a>
</div>