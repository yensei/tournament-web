package py.com.mryensei.tournament.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import py.com.mryensei.tournament.web.domain.Usuario;

@Controller
public class UsuarioController {
	
	@RequestMapping(value="viewListUsers")
	public ModelAndView viewUsers(Model model){
		Map<String, List<Usuario>> usuarios = new HashMap<String, List<Usuario>>();
		usuarios.put("usuarios", Usuario.listAll());
		return new ModelAndView("usuarioList", usuarios);
	}
	@RequestMapping(value="addUser")
	public ModelAndView addUser(@ModelAttribute("user") Usuario usuario){
		Usuario.addUser(usuario);		
		
		Map<String, List<Usuario>> usuarios = new HashMap<String, List<Usuario>>();
		usuarios.put("usuarios", Usuario.listAll());
		return new ModelAndView("usuarioList", usuarios);
	}
}
