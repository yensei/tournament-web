package py.com.mryensei.tournament.web.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;

import py.com.mryensei.tournament.web.general.Configuration;
import py.com.mryensei.tournament.web.general.Services;

public class Usuario implements Serializable {
	
	private static final long serialVersionUID = -4615467429338303108L;
	
	private String nombre, apellido, nick;
	@JsonSerialize(using=DateSerializer.class)
	private Date fechaNacimiento;
	@JsonSerialize(using=DateSerializer.class)
	private Date fechaUltimoLogin;
	@JsonSerialize(using=DateSerializer.class)
	private Date fechaIngreso;
	private String pass;
    private long id;
    private static Logger logger = Logger.getLogger(Usuario.class);
    
    static {
    }
    
    

    public Usuario() {
		super();
	}

	public Usuario(String nombre, String apellido, String nick, long id ) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.nick = nick;
        this.id = id;
        
    }
    
	public static List<Usuario> addUser(Usuario user) {
		
		
		return listAll();
	}
	
	public static List<Usuario> listAll(){
		
		List<Usuario> list = new ArrayList<Usuario>();
		
		RestTemplate restTemplate = new RestTemplate();
		Configuration c = Configuration.getInstance();
		list = restTemplate.getForObject(c.getWsRestURL()+Services.USER_GET_ALL.getWsName(),List.class);
//		Usuario dummy = restTemplate.getForObject(c.getWsRestURL().trim()+Services.USER_GET_DUMMY.getWsName(),Usuario.class);
//		list.add(dummy);
		return list;
	}
	
	@Override
    public String toString()
    {
        return String.format(
            "Usuario [id = %d, nombre = %s, apellido = %s, nick = %s]",
                id,nombre, apellido, nick);
    }

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public Date getFechaUltimoLogin() {
		return fechaUltimoLogin;
	}

	public void setFechaUltimoLogin(Date fechaUltimoLogin) {
		this.fechaUltimoLogin = fechaUltimoLogin;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}
	
}
