package py.com.mryensei.tournament.web.general;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class Configuration {
	
	public static Logger logger = Logger.getLogger(Configuration.class);
	
	private String wsRestURL;
	private static Configuration configuration;
	private String fileUri;

	private Configuration() {
		try {
			fileUri = System.getProperty("jboss.server.config.dir") + "/application.properties";
			Properties prop = new Properties();
			InputStream input = new FileInputStream(fileUri);
			prop.load(input);
			wsRestURL = prop.getProperty("ws.rest.url");
		} catch (FileNotFoundException e) {
			logger.error(e.getMessage(), e);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	public static Configuration getInstance(){
		if(configuration==null){
			configuration = new Configuration(); 
		}
		return configuration;
	}

	public String getWsRestURL() {
		return wsRestURL;
	}
	
}
