package py.com.mryensei.tournament.web.general;

public enum Services {
	
	USER_GET_ALL("/rest/usuarios","GET"),
	USER_ADD("/rest/usuario/create","POST"),
	USER_EDIT("/rest/usuario/edit","POST"),
	USER_REMOVE("/rest/usuario/delete/{id}","GET"),
	USER_GET("/rest/usuario/{id}","GET"),
	USER_GET_DUMMY("/rest/usuario/dummy","GET");
	
	private String wsName;
	private String responseType;

	private Services(String wsName,String responseType) {
		this.wsName = wsName;
		this.responseType = responseType;
	}

	public String getWsName() {
		return wsName;
	}

	public String getResponseType() {
		return responseType;
	}
}
